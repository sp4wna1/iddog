package com.gabriel.prado.iddog.webservice.response

import com.google.gson.annotations.SerializedName

data class FeedResponse(@SerializedName("category") val category: String, @SerializedName("list") val feeds: List<String>)
