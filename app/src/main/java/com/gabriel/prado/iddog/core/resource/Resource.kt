package com.gabriel.prado.iddog.core.resource

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData

sealed class Resource<T>(message: String?, data: T?) {
    data class Loading<T>(val message: String? = null) : Resource<T>(null, null)
    data class Success<T>(val message: String? = null, val data: T) : Resource<T>(message, data)
    data class Error<T>(val message: String? = null) : Resource<T>(message, null)
}

typealias LiveDataResource<T> = LiveData<Resource<T>>

typealias MediatorLiveDataResource<T> = MediatorLiveData<Resource<T>>

