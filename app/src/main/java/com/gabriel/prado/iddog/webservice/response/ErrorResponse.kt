package com.gabriel.prado.iddog.webservice.response

data class ErrorResponse(val error: MessageResponse)
data class MessageResponse(val message: String)
