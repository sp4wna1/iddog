package com.gabriel.prado.iddog.core

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.facebook.stetho.Stetho
import com.gabriel.prado.iddog.core.google.AppExecutor
import com.gabriel.prado.iddog.core.google.AppExecutorImpl
import com.gabriel.prado.iddog.core.imageloader.GlideUseCases
import com.gabriel.prado.iddog.core.imageloader.GlideUseCasesImpl
import com.gabriel.prado.iddog.mvvm.repository.*
import com.gabriel.prado.iddog.mvvm.viewmodel.DogViewModel
import com.gabriel.prado.iddog.mvvm.viewmodel.MainViewModel
import com.gabriel.prado.iddog.webservice.WebService
import com.gabriel.prado.iddog.webservice.WebServiceImpl
import com.gabriel.prado.iddog.webservice.interceptor.TokenInterceptor
import com.gabriel.prado.iddog.webservice.interceptor.TokenInterceptorImpl
import com.gabriel.prado.iddog.webservice.service.FeedService
import com.gabriel.prado.iddog.webservice.service.FeedServiceImpl
import com.gabriel.prado.iddog.webservice.service.LoginService
import com.gabriel.prado.iddog.webservice.service.LoginServiceImpl
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

class CustomApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)

        startKoin(
            this, getModules()
        )
    }


    fun getModules(): List<Module> {
        return listOf(
            webServiceModule(),
            serviceModule(),
            useCasesModule(),
            interceptorModule(),
            executorModule(),
            repositoryModule(),
            viewModelModule(),
            sharedPreferenceModule()
        )
    }

    private fun serviceModule(): Module {
        return module {
            single("feedService") { FeedServiceImpl(get("webService")) as FeedService }
            single("loginService") { LoginServiceImpl(get("webService")) as LoginService }
        }
    }

    private fun webServiceModule(): Module {
        return module {
            single("webService") { WebServiceImpl(get("tokenInterceptor")) as WebService }
        }
    }

    private fun viewModelModule(): Module {
        return module {
            viewModel { MainViewModel(get("loginRepository")) }
            viewModel { DogViewModel(get("dogRepository")) }
        }
    }

    private fun repositoryModule(): Module {
        return module {
            single("dogRepository") { DogRepositoryImpl(get("appExecutor"), get("feedService")) as DogRepository }
            single("loginRepository") {
                LoginRepositoryImpl(
                    get("appExecutor"),
                    get("loginService"),
                    get("sessionManager")
                ) as LoginRepository
            }
            single("sessionManager") { SessionManagerImpl(get("sharedPreference")) as SessionManager }
        }
    }

    private fun executorModule(): Module {
        return module {
            single("appExecutor") { AppExecutorImpl() as AppExecutor }
        }
    }

    private fun useCasesModule(): Module {
        return module {
            single("glide") { GlideUseCasesImpl(applicationContext) as GlideUseCases }
        }
    }

    private fun interceptorModule(): Module {
        return module {
            single("tokenInterceptor") { TokenInterceptorImpl(get("sessionManager")) as TokenInterceptor }
        }
    }

    private fun sharedPreferenceModule(): Module {
        return module {
            single("sharedPreference") { getSharedPreferences("SESSION", Context.MODE_PRIVATE) as SharedPreferences }
        }
    }
}
