package com.gabriel.prado.iddog.mvvm.view.activity

import android.support.v4.view.ViewPager
import com.gabriel.prado.iddog.R
import com.gabriel.prado.iddog.core.BaseActivity
import com.gabriel.prado.iddog.mvvm.view.adapter.DogPagerAdapter
import com.gabriel.prado.iddog.mvvm.viewmodel.DogViewModel
import kotlinx.android.synthetic.main.activity_dog.*
import org.koin.android.ext.android.inject

class DogActivity : BaseActivity() {

    override fun layoutId(): Int = R.layout.activity_dog

    private val dogViewModel: DogViewModel by inject()

    override fun initializeComponents() {
        view_pager.offscreenPageLimit = 2
        view_pager.adapter = DogPagerAdapter(supportFragmentManager)
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                val id: Int
                when (position) {
                    0 -> {
                        id = R.id.husky
                        bottom_navigation.setSelectedItemId(id)
                        bottom_navigation.getMenu().getItem(0).setChecked(true)
                    }
                    1 -> {
                        id = R.id.labrador
                        bottom_navigation.setSelectedItemId(id)
                        bottom_navigation.getMenu().getItem(1).setChecked(true)
                    }
                    2 -> {
                        id = R.id.hound
                        bottom_navigation.setSelectedItemId(id)
                        bottom_navigation.getMenu().getItem(2).setChecked(true)
                    }
                    else -> {
                        id = R.id.pug
                        bottom_navigation.setSelectedItemId(id)
                        bottom_navigation.getMenu().getItem(3).setChecked(true)
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.husky -> {
                    view_pager.currentItem = 0
                    bottom_navigation.menu.getItem(0).isChecked = true
                }
                R.id.labrador -> {
                    view_pager.currentItem = 1
                    bottom_navigation.menu.getItem(1).isChecked = true
                }
                R.id.hound -> {
                    view_pager.currentItem = 2
                    bottom_navigation.menu.getItem(2).isChecked = true
                }
                R.id.pug -> {
                    view_pager.currentItem = 3
                    bottom_navigation.menu.getItem(3).isChecked = true
                }
            }
            false
        }
    }
}
