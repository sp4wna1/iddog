package com.gabriel.prado.iddog.core

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        initializeComponents()
        observeViewModelChanges()
    }

    open fun initializeComponents() {}

    open fun observeViewModelChanges() {}

}
