package com.gabriel.prado.iddog.webservice

import retrofit2.Call
import retrofit2.Response

interface WebService {
    fun <T> execute(request: Call<T>): Response<T>?
    fun <T> createRequests(requestsInterface: Class<T>): T
}

