package com.gabriel.prado.iddog.core.imageloader

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.gabriel.prado.iddog.R
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration

class GlideUseCasesImpl(private val context: Context) : GlideUseCases {

    private val imageLoader = ImageLoader.getInstance()

    init {
        val config = ImageLoaderConfiguration.Builder(context).memoryCache(LruMemoryCache(2 * 1024 * 1024))
            .memoryCacheSize(2 * 1024 * 1024).build()
        imageLoader.init(config)
    }

    override fun loadImage(imageView: ImageView, url: String) {
        Glide.with(context)
            .load(url)
            .apply(RequestOptions.placeholderOf(R.color.black))
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .into(imageView)
    }
}
