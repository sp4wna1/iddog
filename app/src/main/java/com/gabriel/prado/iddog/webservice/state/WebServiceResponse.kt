package com.gabriel.prado.iddog.webservice.state

sealed class WebServiceResponse<T>(message: String?, httpCode: Int?, data: T?) {

    data class Success<T>(val message: String?, val httpCode: Int?, val data: T) :
        WebServiceResponse<T>(message, httpCode, data)

    data class Error<T>(val message: String?, val httpCode: Int?) : WebServiceResponse<T>(message, httpCode, null)
}
