package com.gabriel.prado.iddog.mvvm.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.gabriel.prado.iddog.core.google.SingleLiveEvent
import com.gabriel.prado.iddog.core.resource.LiveDataResource
import com.gabriel.prado.iddog.mvvm.repository.DogRepository
import com.gabriel.prado.iddog.webservice.response.FeedResponse

class DogViewModel(private val dogRepository: DogRepository) : ViewModel() {

    sealed class Action {
        class OpenDogScreen(val url: String) : Action()
    }

    internal val action: SingleLiveEvent<Action> = SingleLiveEvent()

    private val dogCategory: MutableLiveData<String> = MutableLiveData()
    internal val feedLiveData: LiveDataResource<FeedResponse> = Transformations.switchMap(dogCategory) { category ->
        dogRepository.fetchFeed(category)
    }

    fun onDogImageClick(url: String) {
        action.value = Action.OpenDogScreen(url)
    }

    fun updateCategory(dogCategory: String) {
        if (this.dogCategory.value != dogCategory) this.dogCategory.value = dogCategory
    }
}
