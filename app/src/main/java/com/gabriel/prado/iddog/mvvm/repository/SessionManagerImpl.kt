package com.gabriel.prado.iddog.mvvm.repository

import android.content.SharedPreferences

interface SessionManager {
    fun saveToken(token: String?)
    fun getToken(): String?
}

class SessionManagerImpl(private val sharedPreferences: SharedPreferences) : SessionManager {

    companion object {
        private const val TOKEN_KEY = "TOKEN_KEY"
    }

    override fun saveToken(token: String?) { sharedPreferences.edit().putString(TOKEN_KEY, token).apply() }

    override fun getToken(): String? = sharedPreferences.getString(TOKEN_KEY, null)

}
