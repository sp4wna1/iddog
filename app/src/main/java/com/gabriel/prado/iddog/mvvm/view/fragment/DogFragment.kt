package com.gabriel.prado.iddog.mvvm.view.fragment

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.content.DialogInterface
import android.os.Bundle
import com.gabriel.prado.iddog.R
import com.gabriel.prado.iddog.core.BaseFragment
import com.gabriel.prado.iddog.core.gone
import com.gabriel.prado.iddog.core.imageloader.GlideUseCases
import com.gabriel.prado.iddog.core.resource.Resource
import com.gabriel.prado.iddog.core.visible
import com.gabriel.prado.iddog.mvvm.view.activity.PhotoActivity
import com.gabriel.prado.iddog.mvvm.view.adapter.DogAdapter
import com.gabriel.prado.iddog.mvvm.viewmodel.DogViewModel
import com.gabriel.prado.iddog.webservice.response.FeedResponse
import kotlinx.android.synthetic.main.fragment_dog.*
import org.koin.android.ext.android.inject

class DogFragment : BaseFragment() {

    companion object {

        private const val EXTRA_CATEGORY = "EXTRA_CATEGORY"

        fun newInstance(category: String): BaseFragment = DogFragment().apply {
            arguments = Bundle().also {
                it.putString(EXTRA_CATEGORY, category)
            }
        }

        fun getDogCategory(arguments: Bundle?) = arguments?.getString(EXTRA_CATEGORY) ?: "husky"
    }

    private val dogViewModel: DogViewModel by inject()
    private val glide: GlideUseCases by inject()

    private lateinit var adapter: DogAdapter

    override fun setUpView(): Int = R.layout.fragment_dog

    override fun initializeComponents() {
        dogViewModel.updateCategory(getDogCategory(arguments))
        adapter = DogAdapter(glide) { url -> dogViewModel.onDogImageClick(url) }
        dog_list.adapter = adapter
    }

    override fun observeViewModelChanges() {
        observeActionLiveData()
        observeFeedLiveData()
    }

    private fun observeActionLiveData() {
        dogViewModel.action.observe(this, Observer {
            when (it) {
                is DogViewModel.Action.OpenDogScreen -> startActivity(PhotoActivity.newIntent(context, it.url))
            }
        })
    }

    private fun observeFeedLiveData() {
        dogViewModel.feedLiveData.observe(this, Observer {
            when (it) {
                is Resource.Loading -> loadingState()
                is Resource.Success -> successState(it.data)
                is Resource.Error -> errorState(it.message)
            }
        })
    }


    private fun loadingState() {
        progress_bar.visible()
    }

    private fun successState(
        feedResponse: FeedResponse
    ) {
        progress_bar.gone()
        adapter.setData(feedResponse.feeds)
    }

    private fun errorState(message: String?) {
        progress_bar.gone()
        AlertDialog.Builder(context)
            .setPositiveButton(message ?: "Ocorreu um error.") { dialog: DialogInterface?, which: Int ->
                dialog?.dismiss()
            }.create().show()
    }
}
