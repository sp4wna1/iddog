package com.gabriel.prado.iddog.webservice.response

import com.google.gson.annotations.SerializedName
import java.util.*

data class UserResponse(
    @SerializedName("_id") val id: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("token") val token: String?,
    @SerializedName("createdAt") val created: Date?,
    @SerializedName("updatedAt") val updated: Date?,
    @SerializedName("__v") val v: Int?
)
