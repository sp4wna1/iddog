package com.gabriel.prado.iddog.mvvm.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup
import com.gabriel.prado.iddog.mvvm.view.fragment.DogFragment


class DogPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private enum class Dog {
        HUSKY, LABRADOR, HOUND, PUG
    }


    override fun getItem(position: Int): Fragment {
        val category = Dog.values()[position].name.toLowerCase()
        return DogFragment.newInstance(category)
    }

    override fun getCount(): Int = 4

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
    }
}
