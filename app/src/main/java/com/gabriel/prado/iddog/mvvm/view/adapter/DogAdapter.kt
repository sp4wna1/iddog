package com.gabriel.prado.iddog.mvvm.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Space
import com.gabriel.prado.iddog.R
import com.gabriel.prado.iddog.core.gone
import com.gabriel.prado.iddog.core.imageloader.GlideUseCases
import com.gabriel.prado.iddog.core.visible


class DogAdapter(private val glideUseCases: GlideUseCases, private val function: (url: String) -> Unit) :
    RecyclerView.Adapter<DogAdapter.DogViewHolder>() {

    private val dogList: MutableList<String> = mutableListOf()

    fun setData(dogList: List<String>) {
        this.dogList.clear()
        this.dogList.addAll(dogList)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DogViewHolder {
        return DogViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.dog_row, p0, false))
    }

    override fun getItemCount(): Int {
        return dogList.size
    }

    override fun onBindViewHolder(p0: DogViewHolder, p1: Int) {
        p0.bind(p1)
    }


    inner class DogViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {

        private val space = itemView.findViewById<Space>(R.id.space)
        private val image = itemView.findViewById<ImageView>(R.id.dog_image)

        fun bind(p1: Int) {
            if (p1 == dogList.lastIndex) {
                space.visible()
            } else {
                space.gone()
            }

            val url = dogList[p1]
            glideUseCases.loadImage(image, url)
            itemView.setOnClickListener {
                function(url)
            }
        }
    }
}
