package com.gabriel.prado.iddog.mvvm.repository

import android.arch.lifecycle.MediatorLiveData
import com.gabriel.prado.iddog.core.google.AppExecutor
import com.gabriel.prado.iddog.core.resource.LiveDataResource
import com.gabriel.prado.iddog.core.resource.MediatorLiveDataResource
import com.gabriel.prado.iddog.core.resource.Resource
import com.gabriel.prado.iddog.webservice.response.LoginResponse
import com.gabriel.prado.iddog.webservice.service.LoginService
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse


interface LoginRepository {
    fun signUp(email: String): LiveDataResource<LoginResponse>
}

class LoginRepositoryImpl(
    private val appExecutor: AppExecutor,
    private val loginService: LoginService,
    private val sessionManager: SessionManager
) :
    LoginRepository {

    override fun signUp(email: String): LiveDataResource<LoginResponse> {
        val result: MediatorLiveDataResource<LoginResponse> = MediatorLiveData()
        result.postValue(Resource.Loading())

        appExecutor.networkIO {
            val response = loginService.signUp(email)
            if (response is WebServiceResponse.Success) {
                sessionManager.saveToken(response.data.user?.token)
                result.postValue(Resource.Success(response.message, response.data))
            } else if (response is WebServiceResponse.Error) {
                result.postValue(Resource.Error(response.message))
            }
        }
        return result
    }
}
