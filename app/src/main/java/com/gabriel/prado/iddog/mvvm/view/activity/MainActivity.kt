package com.gabriel.prado.iddog.mvvm.view.activity

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.content.DialogInterface
import android.content.Intent
import com.gabriel.prado.iddog.R
import com.gabriel.prado.iddog.core.BaseActivity
import com.gabriel.prado.iddog.core.gone
import com.gabriel.prado.iddog.core.resource.Resource
import com.gabriel.prado.iddog.core.visible
import com.gabriel.prado.iddog.mvvm.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity() {

    private val mainViewModel: MainViewModel by inject()

    override fun layoutId(): Int = R.layout.activity_main

    override fun initializeComponents() {
        sign_up_button.setOnClickListener {
            mainViewModel.onSignUpClick(input_text_email.text.toString())
        }
    }

    override fun observeViewModelChanges() {
        mainViewModel.signUpLiveData.observe(this, Observer {
            when (it) {
                is Resource.Loading -> {
                    loadingState()
                }
                is Resource.Success -> {
                    successState()
                }
                is Resource.Error -> {
                    errorState(it.message)
                }
            }
        })
    }

    private fun loadingState() {
        progress_bar.visible()
    }

    private fun successState() {
        progress_bar.gone()
        startActivity(Intent(this, DogActivity::class.java))
        finish()
    }

    private fun errorState(message: String?) {
        progress_bar.gone()
        AlertDialog.Builder(this)
            .setTitle(R.string.error)
            .setMessage(message ?: getString(R.string.common_error))
            .setPositiveButton(R.string.ok) { dialog: DialogInterface?, which: Int ->
                dialog?.dismiss()
            }.create().show()
    }
}
