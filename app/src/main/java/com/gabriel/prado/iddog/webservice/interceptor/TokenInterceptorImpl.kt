package com.gabriel.prado.iddog.webservice.interceptor

import com.gabriel.prado.iddog.mvvm.repository.SessionManager
import okhttp3.Interceptor
import okhttp3.Response

interface TokenInterceptor : Interceptor

class TokenInterceptorImpl(private val sessionManager: SessionManager) : TokenInterceptor {

    companion object {
        private const val TOKEN = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        val token = sessionManager.getToken()

        if (token != null) {
            requestBuilder.addHeader(TOKEN, token)
        }
        return chain.proceed(requestBuilder.build())
    }
}
