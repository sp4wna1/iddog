package com.gabriel.prado.iddog.webservice.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(@SerializedName("user") val user: UserResponse?)

