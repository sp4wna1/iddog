package com.gabriel.prado.iddog.mvvm.repository

import com.gabriel.prado.iddog.core.google.AppExecutor
import com.gabriel.prado.iddog.core.resource.LiveDataResource
import com.gabriel.prado.iddog.core.resource.MediatorLiveDataResource
import com.gabriel.prado.iddog.core.resource.Resource
import com.gabriel.prado.iddog.webservice.response.FeedResponse
import com.gabriel.prado.iddog.webservice.service.FeedService
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse

interface DogRepository {
    fun fetchFeed(category: String? = "husky"): LiveDataResource<FeedResponse>
}

class DogRepositoryImpl(private val appExecutor: AppExecutor, private val feedService: FeedService) : DogRepository {

    override fun fetchFeed(category: String?): LiveDataResource<FeedResponse> {
        val result: MediatorLiveDataResource<FeedResponse> = MediatorLiveDataResource()
        result.postValue(Resource.Loading())

        appExecutor.networkIO {
            val response = feedService.fetchFeed(category)
            if (response is WebServiceResponse.Success) {
                result.postValue(Resource.Success(response.message, response.data))
            } else if (response is WebServiceResponse.Error) {
                result.postValue(Resource.Error(response.message))
            }
        }

        return result
    }
}
