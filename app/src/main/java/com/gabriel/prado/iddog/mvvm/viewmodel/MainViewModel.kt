package com.gabriel.prado.iddog.mvvm.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.gabriel.prado.iddog.core.resource.LiveDataResource
import com.gabriel.prado.iddog.mvvm.repository.LoginRepository
import com.gabriel.prado.iddog.webservice.response.LoginResponse

class MainViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val userEmail: MutableLiveData<String> = MutableLiveData()

    internal val signUpLiveData: LiveDataResource<LoginResponse> =
        Transformations.switchMap(userEmail) { email ->
            loginRepository.signUp(email)
        }

    fun onSignUpClick(email: String) {
        userEmail.postValue(email)
    }

}
