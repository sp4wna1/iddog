package com.gabriel.prado.iddog.core.google

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors


interface AppExecutor {
    fun diskIO(f: () -> Unit)
    fun networkIO(f: () -> Unit)
    fun mainThread(f: () -> Unit)
}


class AppExecutorImpl(
    private val diskIO: Executor = Executors.newSingleThreadExecutor(),
    private val networkIO: Executor = Executors.newFixedThreadPool(3),
    private val mainThread: Executor = MainThreadExecutor()
) : AppExecutor {

    override fun diskIO(f: () -> Unit) {
        diskIO.execute {
            f()
        }
    }

    override fun networkIO(f: () -> Unit) {
        networkIO.execute {
            f()
        }
    }

    override fun mainThread(f: () -> Unit) {
        mainThread.execute {
            f()
        }
    }

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}
