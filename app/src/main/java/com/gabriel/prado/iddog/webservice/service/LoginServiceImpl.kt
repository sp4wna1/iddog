package com.gabriel.prado.iddog.webservice.service

import com.gabriel.prado.iddog.webservice.WebService
import com.gabriel.prado.iddog.webservice.response.ErrorResponse
import com.gabriel.prado.iddog.webservice.response.LoginResponse
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface LoginService {
    fun signUp(email: String): WebServiceResponse<LoginResponse>
}

class LoginServiceImpl(private val webService: WebService) : LoginService {

    private val request = webService.createRequests(Request::class.java)

    override fun signUp(email: String): WebServiceResponse<LoginResponse> {
        val response = webService.execute(request.signUp(email))
        val body = response?.body()

        return if (response != null && response.isSuccessful && body != null) {
            WebServiceResponse.Success(response.message(), response.code(), body)
        } else {
            var message = response?.message()
            if (message.isNullOrEmpty()) {
                message = Gson().fromJson(response?.errorBody()?.string(), ErrorResponse::class.java).error.message
            }
            WebServiceResponse.Error(message, response?.code())
        }
    }

    private interface Request {

        @FormUrlEncoded
        @POST("/signup")
        fun signUp(@Field("email") email: String): Call<LoginResponse>

    }
}
