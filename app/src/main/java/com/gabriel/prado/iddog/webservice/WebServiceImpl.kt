package com.gabriel.prado.iddog.webservice

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.gabriel.prado.iddog.webservice.interceptor.TokenInterceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WebServiceImpl(private val tokenInterceptor: TokenInterceptor) : WebService {

    private val retrofit: Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api-iddog.idwall.co")
            .client(getOkHttpClient())
            .build()

    private fun getOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .addInterceptor(tokenInterceptor)
            .build()

    override fun <T> execute(request: Call<T>): Response<T>? {
        return request.execute()
    }

    override fun <T> createRequests(requestsInterface: Class<T>): T = retrofit.create(requestsInterface)
}
