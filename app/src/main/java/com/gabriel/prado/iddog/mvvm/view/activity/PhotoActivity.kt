package com.gabriel.prado.iddog.mvvm.view.activity

import android.content.Context
import android.content.Intent
import com.gabriel.prado.iddog.R
import com.gabriel.prado.iddog.core.BaseActivity
import com.gabriel.prado.iddog.core.imageloader.GlideUseCases
import kotlinx.android.synthetic.main.activity_photo.*
import org.koin.android.ext.android.inject

class PhotoActivity : BaseActivity() {

    companion object {

        private const val EXTRA_ITEM_IMAGE_URL = "EXTRA_ITEM_IMAGE_URL"

        fun newIntent(context: Context?, imageUrl: String): Intent {
            return Intent(context, PhotoActivity::class.java).apply {
                putExtra(EXTRA_ITEM_IMAGE_URL, imageUrl)
            }
        }
    }

    private val glide: GlideUseCases by inject()

    override fun layoutId(): Int = R.layout.activity_photo

    override fun initializeComponents() {
        intent.getStringExtra(EXTRA_ITEM_IMAGE_URL)?.let { url ->
            glide.loadImage(photo, url)
        }
    }
}
