package com.gabriel.prado.iddog.webservice.service

import com.gabriel.prado.iddog.webservice.WebService
import com.gabriel.prado.iddog.webservice.response.FeedResponse
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


interface FeedService {
    fun fetchFeed(category: String?): WebServiceResponse<FeedResponse>
}

class FeedServiceImpl(private val webService: WebService) : FeedService {

    private val request = webService.createRequests(Request::class.java)

    override fun fetchFeed(category: String?): WebServiceResponse<FeedResponse> {
        val response = webService.execute(request.fetchFeed(category))
        val body = response?.body()

        return if (response != null && response.isSuccessful && body != null) {
            WebServiceResponse.Success(response.message(), response.code(), body)
        } else {
            WebServiceResponse.Error(response?.message(), response?.code())
        }

    }

    private interface Request {
        @Headers("Content-Type: application/json")
        @GET("feed")
        fun fetchFeed(@Query("category") category: String?): Call<FeedResponse>
    }
}
