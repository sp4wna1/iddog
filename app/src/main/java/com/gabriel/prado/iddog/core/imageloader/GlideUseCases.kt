package com.gabriel.prado.iddog.core.imageloader

import android.widget.ImageView

interface GlideUseCases {

    fun loadImage(imageView: ImageView, url : String)
}
