package com.gabriel.prado.iddog.core.google

import android.os.Handler
import android.os.Looper
import com.gabriel.prado.iddog.IOIdlingResource
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AppExecutorImpl(
    private val diskIO: Executor = Executors.newSingleThreadExecutor(),
    private val networkIO: Executor = Executors.newFixedThreadPool(3),
    private val mainThread: Executor = MainThreadExecutor()
) : AppExecutor {

    override fun diskIO(f: () -> Unit) {
        diskIO.execute {
            IOIdlingResource.idle = false
            f()
            IOIdlingResource.idle = true
        }
    }

    override fun networkIO(f: () -> Unit) {
        networkIO.execute {
            IOIdlingResource.idle = false
            f()
            IOIdlingResource.idle = true
        }
    }

    override fun mainThread(f: () -> Unit) {
        mainThread.execute {
            IOIdlingResource.idle = false
            f()
            IOIdlingResource.idle = true
        }
    }

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}
