package com.gabriel.prado.iddog

import android.support.test.espresso.IdlingResource

object IOIdlingResource : IdlingResource {

    var idle = true

    private lateinit var callback: IdlingResource.ResourceCallback

    override fun getName() = "IOIdlingResource"

    override fun isIdleNow() = idle.also {
        if (it) callback.onTransitionToIdle()
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.callback = callback
    }
}
