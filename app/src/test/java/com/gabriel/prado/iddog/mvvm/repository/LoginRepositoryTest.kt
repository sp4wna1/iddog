package com.gabriel.prado.iddog.mvvm.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gabriel.prado.iddog.core.google.AppExecutor
import com.gabriel.prado.iddog.core.resource.Resource
import com.gabriel.prado.iddog.webservice.response.LoginResponse
import com.gabriel.prado.iddog.webservice.response.UserResponse
import com.gabriel.prado.iddog.webservice.service.LoginService
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse.Error
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse.Success
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.koin.test.declareMock
import org.mockito.Mockito.*
import java.util.*
import java.util.concurrent.Executor

class LoginRepositoryTest : KoinTest {

    @get:Rule
    var instantTask = InstantTaskExecutorRule()

    private val appExecutor: AppExecutor by inject()
    private val loginService: LoginService by inject()
    private val sessionManager: SessionManager by inject()

    private lateinit var loginRepository: LoginRepository

    @Before
    fun setUp() {
        declareMock<AppExecutor>()
        declareMock<LoginService>()
        declareMock<SessionManager>()

        loginRepository = LoginRepositoryImpl(appExecutor, loginService, sessionManager)

        `when`(appExecutor.networkIO()).thenReturn(Executor { it.run() })
    }

    @Test
    fun `GIVEN successResult WHEN signUp is called THEN return Success`() {
        val loginResponse = LoginResponse(UserResponse("123", "test@test.com", "Token", Date(), Date(), 0))

        `when`(loginService.signUp(anyString())).thenReturn(Success(null, 200, loginResponse))

        val result = loginRepository.signUp("test@test.com")
        verify(loginService).signUp("test@test.com")
        verify(sessionManager).saveToken("Token")

        assertEquals(Resource.Success(data = loginResponse), result.value)
    }


    @Test
    fun `GIVEN errorResult WHEN signUp is called THEN return error`() {
        `when`(loginService.signUp(anyString())).thenReturn(Error("error", 400))

        val result = loginRepository.signUp("test@test.com")
        verify(loginService).signUp("test@test.com")

        assertEquals(Resource.Error<LoginResponse>("error"), result.value)
    }
}
