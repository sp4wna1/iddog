package com.gabriel.prado.iddog.mvvm.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gabriel.prado.iddog.core.google.AppExecutor
import com.gabriel.prado.iddog.core.resource.Resource
import com.gabriel.prado.iddog.webservice.response.FeedResponse
import com.gabriel.prado.iddog.webservice.service.FeedService
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse.Error
import com.gabriel.prado.iddog.webservice.state.WebServiceResponse.Success
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.koin.test.declareMock
import org.mockito.Mockito.*
import java.util.concurrent.Executor

class DogRepositoryTest : KoinTest {

    @get:Rule
    var instantTask = InstantTaskExecutorRule()

    private val appExecutor: AppExecutor by inject()
    private val feedService: FeedService by inject()

    private lateinit var dogRepository: DogRepository

    @Before
    fun setUp() {
        declareMock<AppExecutor>()
        declareMock<FeedService>()

        dogRepository = DogRepositoryImpl(appExecutor, feedService)

        `when`(appExecutor.networkIO()).thenReturn(Executor { it.run() })
    }

    @Test
    fun `GIVEN successResult WHEN fetchFeed is called THEN return Success`() {
        val feedResponse = FeedResponse("husky", listOf("www.test.com"))
        `when`(feedService.fetchFeed(anyString())).thenReturn(Success(null, 200, feedResponse))

        val result = dogRepository.fetchFeed()
        verify(feedService).fetchFeed("husky")

        assertEquals(Resource.Success(data = feedResponse), result.value)
    }


    @Test
    fun `GIVEN errorResult WHEN fetchFeed is called THEN return error`() {
        `when`(feedService.fetchFeed(anyString())).thenReturn(Error("error", 400))

        val result = dogRepository.fetchFeed()
        verify(feedService).fetchFeed("husky")

        assertEquals(Resource.Error<FeedResponse>("error"), result.value)
    }
}
